from flask import jsonify, Flask, request, redirect,url_for
import numpy as np
from saxpy.znorm import znorm
from saxpy.sax import ts_to_string
from saxpy.alphabet import cuts_for_asize
from saxpy.paa import paa
from Levenshtein import distance #pip install python-Levenshtein


app = Flask(__name__)

@app.route('/', methods =['GET'])
def index():
    return(jsonify({'template':'template', 'source': "ok"}))

# sax?inputSeries=2.1,3.3,4.4,5.5,6.6,7.7&segmentation=10 
@app.route('/sax', methods = ['POST', 'GET']) #it's all URI param based so we'll include get
def stringToSax():

    inputSeriesParam = request.args.get('inputSeries')
    inputseries =  map(str.strip, str(inputSeriesParam).split(','))
    cuts = int(request.args.get('segmentation'))

    npArr = np.array(inputseries).astype(np.float)
    znormed = znorm(npArr)
    x = ts_to_string(znormed, cuts_for_asize(cuts))

    return  jsonify(
    {
      'inputSeries':inputseries,
      'segmentation':cuts,
      'sax-string':x,
    }) 

# comparesax?templateSeries=2.1,3.3,4.4,5.5,6.6,7.7&sampleSeries=7.7,1,3.3,4.4,5.5,6.6,7.7&segmentation=10 
# comparesax?templateSeries=2.1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3.3,4.4,5.5,6.6,7.7&sampleSeries=7.7,666,6,6,6,6,6,3.3,4.4,5.5,6.6,7.7&segmentation=10
@app.route('/comparesax', methods = ['POST', 'GET'])
def compareSaxStrings():
    templateSeriesParam = request.args.get('templateSeries')
    sampleSeriesParam = request.args.get('sampleSeries')
    cuts = int(request.args.get('segmentation'))

    subst = "http"
    if str(templateSeriesParam).find(subst) != -1:
        #GET data
        print("need to get template data")
        templateSeries = 0
    else:
        print("template data is ok")
        templateSeries =  map(str.strip, str(templateSeriesParam).split(','))

    subst = "http"
    if str(sampleSeriesParam).find(subst) != -1:
        #GET data
        print("need to get sample data")
        sampleSeries = 0
    else:
        print("sample data is ok")
        sampleSeries =  map(str.strip, str(sampleSeriesParam).split(','))

    tnpArr = np.array(templateSeries).astype(np.float)
    tznormed = znorm(tnpArr)
    tx = ts_to_string(tznormed, cuts_for_asize(cuts))


    snpArr = np.array(sampleSeries).astype(np.float)
    sznormed = znorm(snpArr)
    sx = ts_to_string(sznormed, cuts_for_asize(cuts))


    #levensteinFunction comparison
    result = distance(tx,sx)
    return  jsonify(
    {
      'templateSeries':templateSeries,
      'sampleSeries':sampleSeries,
      'segmentation':cuts,
      'sax-string-template':tx,
      'sax-string-sample':sx,
      'comparison-results' : result
    })


# set the debug to false when testing is finished
if __name__ == '__main__':
    app.run()